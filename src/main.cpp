#include <Arduino.h>

void setup() {
  Serial.begin(9600);
}

void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  float maxAnalogVoltageInput = 3.3;
  int adcResolution10bit = 1023;
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (maxAnalogVoltageInput / adcResolution10bit);
  // print out the value you read:
  Serial.print("Raw:");
  Serial.print(sensorValue);
  Serial.print(" , phys:");
  Serial.print(voltage);
  Serial.println("V.");
}